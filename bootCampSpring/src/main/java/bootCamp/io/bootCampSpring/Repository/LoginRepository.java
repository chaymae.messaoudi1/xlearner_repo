package bootCamp.io.bootCampSpring.Repository;

import bootCamp.io.bootCampSpring.beans.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract class LoginRepository implements JpaRepository<Login,Integer> {
}
