package bootCamp.io.bootCampSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootCampSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootCampSpringApplication.class, args);
	}

}
