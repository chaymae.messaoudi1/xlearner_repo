function WelcomeFunc ({name, children}){
    return <div>
        <h1> hello {name}</h1>
        <p> {children}</p>
    </div>
}

class Welcome extends ReadableByteStreamController.Component {
    render(){
        return <div>
        <h1> hello {this.this.props.name}</h1>
        <p> {this.props.children}</p>
    </div>
    }
}

ReactDOM.render(<Welcome name="chaymae">hello hello</Welcome>)
