package io.projet.xlearning.xhub.projet.xlearning.Topic;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Topic {
    @Id
    private String id;
    private String name;
    private String description;

    public Topic(String id, String name, String description){
        super();
        this.id = id;
        this.name = name;
        this.description = description;
    }
    public String getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description= description;
    }


}
